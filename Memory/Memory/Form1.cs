﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Memory
{
    public partial class Form1 : Form
    {
        Random rand = new Random();
        long czas = 0;
        List<string> obrazki = new List<string>()
        {
            "a", "a", "b", "b","c","c","d","d","e","e","f","f","g","g","h","h"
        };
        Label pierwszyWyb, drugiWyb;
        public Form1()
        {
            InitializeComponent();
            
            MessageBox.Show("Proszę o ustawienie elementów. Gdy będziesz gotów wciśnij START");
            Gracze.ZPliku(textBox1);
            Generuj();
            
        }

        private void label_Click(object sender, EventArgs e)
        {
            Label label = sender as Label;
            if (pierwszyWyb != null && drugiWyb != null) return;
            if (label == null) return;
            if (label.ForeColor != label.BackColor) return;
            if (pierwszyWyb == null)
            {
                pierwszyWyb = label;
                pierwszyWyb.ForeColor = pictureBox2.BackColor;
                return;
            }
            drugiWyb = label;
            drugiWyb.ForeColor = pictureBox2.BackColor;
            
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            if (pierwszyWyb.Text == drugiWyb.Text)
            {
               
                pierwszyWyb.ForeColor = Color.Green;
                drugiWyb.ForeColor = Color.Green;
                CzyKoniec();               
            }
            else
            {
                pierwszyWyb.ForeColor = pierwszyWyb.BackColor;
                drugiWyb.ForeColor = drugiWyb.BackColor;
            }
            pierwszyWyb = null;
            drugiWyb = null;
        }

        private void Generuj()
        {
            Label label;
            int losowy;
            for (int i = 0; i < tableLayoutPanel1.Controls.Count; i++)
            {
                label = tableLayoutPanel1.Controls[i] as Label;
                losowy = rand.Next(0,obrazki.Count);
                label.Text = obrazki[losowy];
                obrazki.RemoveAt(losowy);
                        
                
               

            }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            czas++;
            label22.Text = Convert.ToString(czas) + " s";
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            pictureBox1.BackColor = colorDialog1.Color;
            Label label;
            tableLayoutPanel1.BackColor = pictureBox1.BackColor;
            for (int i = 0; i < tableLayoutPanel1.Controls.Count; i++)
            {
                label = tableLayoutPanel1.Controls[i] as Label;
                label.BackColor = pictureBox1.BackColor;

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            pictureBox2.BackColor = colorDialog1.Color;
          
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Label label;
            for (int i = 0; i < tableLayoutPanel1.Controls.Count; i++)
            {
                label = tableLayoutPanel1.Controls[i] as Label;
                label.ForeColor = pictureBox1.BackColor;

            }
            button3.Enabled = false;
            button1.Enabled = false;
            button2.Enabled = false;
            button4.Enabled = false;
            listBox1.Enabled = false;
            textBox2.Enabled = false;
            tableLayoutPanel1.Visible = true;
            timer2.Start();
            label21.Visible = true;
            label22.Visible = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            listBox1.Items.Add( textBox2.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            listBox1.Items.Remove(listBox1.SelectedItem);
        }

        private void CzyKoniec()
        {
            Label label;
            for (int i = 0; i < tableLayoutPanel1.Controls.Count; i++)
            {
                label = tableLayoutPanel1.Controls[i] as Label;
                if (label != null && label.ForeColor != Color.Green) return;

            }
            timer2.Stop();
            Gracze.DoPliku(Convert.ToString(czas),Convert.ToString(listBox1.SelectedItem));
            MessageBox.Show("Wygrana!");
        }
        
    }
}
